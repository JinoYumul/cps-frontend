//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable
let courseId = params.get('courseId')

//get the token from localStorage
let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http://localhost:3000/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    // console.log(data);

    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;
    enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>`;

    document.querySelector("#enrollButton").addEventListener("click", () => {
      // insert the course to our courses collection
      fetch("http://localhost:3000/api/users/enroll", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          courseId: courseId,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          //creation of new course successful
          if (data === true) {
            //redirect to courses page
            alert("Thank you for enrolling! See you!");
            window.location.replace("./courses.html");
          } else {
            //error in creating course
            alert("Something went wrong");
          }
        });
    });
  });